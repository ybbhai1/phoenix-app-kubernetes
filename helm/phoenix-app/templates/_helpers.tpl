{{- define "common.name" -}}
{{- default .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- end}}

{{/* Labels */}}
{{- define "common.labels" -}}
{{- range $key, $value := .Values.labels.extra }}
{{- (printf "%s: %s" $key $value) }}
{{- end }}
release: {{ include "common.name" . }}
app.kubernetes.io/name: {{ .Values.labels.required.app }}
app.kubernetes.io/version: {{ .Values.labels.required.version }}
app.kubernetes.io/instance: {{ include "common.name" . }}
app.kubernetes.io/component: {{ .Values.labels.required.component }}
app.kubernetes.io/managed-by: Helm
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
{{- end }}


{{/* matchLabels - helm ensures release name is unique */}}
{{- define "common.matchLabels" -}}
release: {{ include "common.name" . }}
{{- end }}

{{- define "phoenix.selector" -}}
release={{ include "common.name" . }}
{{- end}}

{{/* Generate image pull secret from credentials */}}
{{- define "imagePullSecret" }}
{{- if .Values.image.private }}
{{- with .Values.image.credentials }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"email\":\"%s\",\"auth\":\"%s\"}}}" .registry .username .password .email (printf "%s:%s" .username .password | b64enc) | b64enc }}
{{- end }}
{{- end }}
{{- end }}

