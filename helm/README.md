# Deployment charts

We use the deployment charts to deploy our database and phoenix app.

## Database

With this command we can deploy a postgres database as a statefulset in a kubernetes cluster.
Note that using statefulsets as databases in production is **not recommended**. This is for demonstration purposes only.

We are creating both a root user `dev` for the maintenance database `postgres`, and another
database called `phoenix` with the owner `phoenix_user` by using a [init script](database/scripts/init.sh).
The use of this init script is not required but demonstrate how you can create multiple databases using
init scripts in case you have multiple Repo's. Also the script allows us to change the database user on successive deployments.

```bash
namespace=demo
release=demo-db
helm upgrade $release ./helm/database --install \
--set environment.POSTGRES_DB="postgres" \
--set environment.POSTGRES_USER="dev" \
--set environment.POSTGRES_PASSWORD="dev" \
--set environment.DATABASE_NAME="phoenix" \
--set environment.DATABASE_USER="phoenix_user" \
--set environment.DATABASE_PASSWORD="phoenix_password" \
--set serviceName="phoenix-db-service" \
-n $namespace --create-namespace
```

A simpler alternative which ignores the init script and initializes the demo database.

```bash
namespace=demo
release=demo-db
helm upgrade $release ./helm/database --install \
--set environment.POSTGRES_DB="phoenix" \
--set environment.POSTGRES_USER="phoenix_user" \
--set environment.POSTGRES_PASSWORD="phoenix_password" \
--set deployment.postgresInitScript="false" \
--set serviceName="phoenix-db-service" \
-n $namespace --create-namespace
```

## Phoenix App

This chart helps you deploy the phoenix application we have configured in this project to a kubernetes cluster.
First, before you can deploy, you need to build the app into a docker image and load it into the kind cluster.

```bash
docker build -t phoenix-demo:1.0.0 -f "src/Dockerfile.prod" "./src"
kind load docker-image phoenix-demo:1.0.0 --name phoenix-cluster
```

After that we can deploy the application. The variables `SECRET_KEY_BASE` and `DATABASE_URL` should not be in cleartext in your production environments.

```bash
namespace="demo"
release="demo-app"
version="1.0.0"
helm upgrade $release ./helm/phoenix-app --install \
--set environment.SECRET_KEY_BASE="Xu8gcFbCdq5N41GuWFIhbeiKcoFk0agnPmse4JKycYKADhCBHxmXXCCLnHdUSUZn" \
--set environment.DATABASE_URL="postgresql://phoenix_user:phoenix_password@phoenix-db-service/phoenix" \
--set ingress.enabled="true" \
--set ingress.host="localhost" \
--set image.repository="phoenix-demo" \
--set image.pullPolicy="Never" \
--set image.tag="$version" \
--set labels.required.version="$version" \
--set ecto.migrate="true" \
-n $namespace --create-namespace
```

We set the pull policy to never since we are loading the images into the cluster nodes.

With ingress enabled and host set to localhost, the app will be exposed on port 8000 ([http://localhost:8000](http://localhost:8000) in browser). Note that if you change the ingress endpoint from `web` to something else, the kind cluster configuration should be updated to reflect that. Otherwise the port mapping from docker to local will no longer work.
