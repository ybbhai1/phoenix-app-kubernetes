# Kind cluster

We can use a local kubernetes cluster running in docker containers to test our applications kubernetes configuration and helm charts.

### Create cluster

The following commands should set up and configure the cluster for us.

```bash
# Create a cluster
kind create cluster --config ./kubernetes/kind.config.yaml --name phoenix-cluster
kubectl config set-context kind-phoenix-cluster
# Install traefik on cluster
helm repo add traefik https://helm.traefik.io/traefik
helm repo update
helm install traefik traefik/traefik --version "24.0.0" -n traefik --create-namespace -f ./kubernetes/traefik.values.yaml
```

### Remove cluster

The following command removes the cluster

```bash
kind delete cluster --name phoenix-cluster
```
