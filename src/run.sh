#!/bin/bash

# This is only for the docker-compose setup
mix deps.get
mix deps.compile
mix ecto.setup

if [ -f "assets/package.json" ]; then
  cd assets 
  npm install
  cd ..
fi;

echo ""
echo "RUNNING SERVER WITH 'mix phx.server'"
echo ""

mix phx.server