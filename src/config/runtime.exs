import Config

# ADDED - The entire file is modified (you can ignore the docker_compose_env configuration)

if System.get_env("PHX_SERVER") do
  config :demo, DemoWeb.Endpoint, server: true
end

if config_env() == :prod do
  k8s_namespace =
    System.get_env("K8S_NAMESPACE") ||
      raise """
      Environment variable K8S_NAMESPACE is missing.
      This is the kubernetes namespace the app is deployed to.
      For example: my-namespace
      """

  k8s_selector =
    System.get_env("K8S_SELECTOR") ||
      raise """
      Environment variable K8S_SELECTOR is missing.
      This is the kubernetes namespace the app is deployed to.
      For example: app=demo,version=1.1.0
      """

  k8s_pod_ip =
    System.get_env("K8S_POD_IP") ||
      raise """
      Environment variable K8S_POD_IP is missing.
      This is the IP address of the kubernetes pod running the application.
      For example: 10.0.0.7
      """

  app_host = System.get_env("PHX_HOST", "0.0.0.0")

  app_port =
    String.to_integer(
      System.get_env("PHX_PORT") ||
        raise("""
        Environment variable PORT is missing. This is the port the phoenix application runs on internally.
        For example: 4000
        """)
    )

  secret_key_base =
    System.get_env("SECRET_KEY_BASE") ||
      raise """
      Environment variable SECRET_KEY_BASE is missing.
      You can generate one by calling: mix phx.gen.secret
      """

  database_url =
    System.get_env("DATABASE_URL") ||
      raise """
      Environment variable DATABASE_URL is missing.
      For example: postgres://<user>:<password>@<host>:<port>/<database_name>
      """

  config :kubernetes,
    enabled: true,
    namespace: k8s_namespace,
    selector: k8s_selector,
    pod_ip: k8s_pod_ip

  config :demo, Demo.Repo,
    url: database_url,
    pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10"),
    socket_options: if(System.get_env("ECTO_IPV6"), do: [:inet6], else: [])

  config :demo, DemoWeb.Endpoint,
    url: [host: app_host, port: app_port],
    http: [
      ip: {0, 0, 0, 0, 0, 0, 0, 0},
      port: app_port
    ],
    secret_key_base: secret_key_base
end

# You can ignore this, docker compose is not really a good choice for phoenix project development IMO
# But it is great to test containerization as is the case in kubernetes

if config_env() == :compose do
  database_url =
    System.get_env("DATABASE_URL") ||
      raise """
      Environment variable DATABASE_URL is missing.
      For example: postgres://<user>:<password>@<host>:<port>/<database_name>
      """

  app_port =
    String.to_integer(
      System.get_env("PORT") ||
        raise("""
        Environment variable PORT is missing. This is used to set the port the phoenix application runs on internally.
        For example: 4000
        """)
    )

  config :demo, Demo.Repo, url: database_url
  config :demo, DemoWeb.Endpoint, http: [ip: {0, 0, 0, 0}, port: app_port]
end
