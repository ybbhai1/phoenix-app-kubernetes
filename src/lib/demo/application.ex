defmodule Demo.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  require Logger
  use Application

  # ADDED - method to fetch topology for kubernetes
  @spec kubernetes_topology :: list
  def kubernetes_topology do
    namespace = Application.get_env(:kubernetes, :namespace)
    selector = Application.get_env(:kubernetes, :selector)

    [
      k8s: [
        strategy: Elixir.Cluster.Strategy.Kubernetes,
        config: [
          mode: :dns,
          # Must match name of your app in release
          kubernetes_node_basename: "demo",
          kubernetes_selector: selector,
          kubernetes_namespace: namespace,
          polling_interval: 10_000
        ]
      ]
    ]
  end

  @impl true
  def start(_type, _args) do
    k8s = Application.get_env(:kubernetes, :enabled, false)
    if k8s, do: Logger.info("Kubernetes enabled, starting cluster supervisor")

    children =
      [
        # Start the Telemetry supervisor
        DemoWeb.Telemetry,
        # Start the Ecto repository
        Demo.Repo,
        # Start the PubSub system
        {Phoenix.PubSub, name: Demo.PubSub},
        # Start Finch
        {Finch, name: Demo.Finch},
        # Start the Endpoint (http/https)
        DemoWeb.Endpoint
        # Start a worker by calling: Demo.Worker.start_link(arg)
        # {Demo.Worker, arg}
      ]
      |> append_if(
        k8s,
        {Cluster.Supervisor, [kubernetes_topology(), [name: Demo.ClusterSupervisor]]}
      )

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Demo.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # ADDED - Helper function to add configuration to application given a condition
  defp append_if(list, condition, item) do
    if condition, do: list ++ [item], else: list
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    DemoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
