# Phoenix with kubernetes

This is a DevOps enabled, phoenix with kubernetes project example. It has the following features.

- Phoenix project set up for local development and docker-compose
- Phoenix set up for production kuberentes
- Kind kubernetes cluster for local development
- Helm charts for deploying our phoenix app and a database

## Preface

This project was created using an ubuntu machine. I can make no guarantees of it's legitimacy on other operating systems.
However, I have set up docker and docker-compose files to try and make this as OS independent as possible.

How you go about installing the required dependencies will vary depending on the OS you are using.
I am using linux (ubuntu 22.04), so the guide I provide will assume you are also.

## Prerequisites

- Docker (docker.io package in linux)
- Docker compose (docker-compose package in linux)

## Starting project

At this point you should be able to start the project with `docker-compose up`.

- Phoenix Application at [http://localhost:4000](http://localhost:4000)
- SMTP Server frontend at [http://localhost:4100](http://localhost:4100)
- Database on localhost port 6432

If you close out of the docker-compose runtime, you can start your containers again with `bash docker_start.sh`

## Getting started

1. Install [ASDF](https://asdf-vm.com/guide/getting-started.html) to help manage our development dependencies
2. Add the required plugins

```bash
# Elixir: Needs unzip to be installed (See https://github.com/asdf-vm/asdf-elixir#install)
asdf plugin add elixir https://github.com/asdf-vm/asdf-elixir.git
# Erlang: Needs apt packages to be installed (See https://github.com/asdf-vm/asdf-erlang#before-asdf-install)
asdf plugin add erlang https://github.com/asdf-vm/asdf-erlang.git
# NodeJS: https://github.com/asdf-vm/asdf-nodejs
asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git
# Golang: Needs coreutils to be installed (See https://github.com/asdf-community/asdf-golang#requirements)
asdf plugin add golang https://github.com/asdf-community/asdf-golang.git
# Helm: https://github.com/Antiarchitect/asdf-helm
asdf plugin add helm https://github.com/Antiarchitect/asdf-helm.git
# Kubectl: https://github.com/asdf-community/asdf-kubectl
asdf plugin add kubectl https://github.com/asdf-community/asdf-kubectl.git
```

3. Install the plugins with `asdf install`. It will use the plugin versions from [.tool-versions](.tool-versions), which is a file created by asdf.
4. Install [Kind](https://kind.sigs.k8s.io/). Run `asdf reshim golang` after installation so that the `kind` command is recognized.

### Cluster setup

You can spin up the entire project in a cluster with your production configuration by running the [cluster_up](cluster_up.sh) script.

```bash
bash cluster_up.sh
```

- Your application becomes available at [http://localhost:8000](http://localhost:8000)
- Traefik dashboard becomes available at: [http://localhost:9000/dashboard/#/](http://localhost:9000/dashboard/#/)

You can spin it down again with the [cluster_down](cluster_down.sh) script.

```bash
bash cluster_down.sh
```

### What's next?

Each folder within this project has it's own readme with some instructions or useful information in general.
Here's a recommended order to follow

1. [Set up your application](src)
2. [Create a development cluster](kind)
3. [Deploy your application locally](helm)
4. [Add CI/CD pipelines](ci)
